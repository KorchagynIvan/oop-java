/**
 * Created by Jesus Christ on 17.01.2017.
 */
import java.util.Vector;
import java.util.Random ;
public class Field {
    private int size;
    private Ancestor[][] amount;
    private Vector<Coord> Curr_alive = new Vector();
    private Vector<Coord> Next_alive = new Vector();
    ;

    Field(int sizef) {
        Random rand = new Random();
        size = sizef;
        amount = new Ancestor[size][];



        for (int w = 0; w < sizef; w++) {
            amount[w] = new Ancestor[sizef];
            for (int h = 0; h < sizef; h++) {
                amount[w][h] = new Cell();
                amount[w][h].SetState(rand.nextBoolean());
                if (amount[w][h].GetState() == true) {
                    Curr_alive.add(new Coord(w, h));
                }
            }
        }
    }


    void next_gen() {


        for (int l = 0; l < Curr_alive.size(); l++) {
            int w = Curr_alive.get(l).line;
            int h = Curr_alive.get(l).coll;

            for (int wn = (w - 1); wn <= (w + 1); wn++) {//ïðîõîäèìñÿ ïî æèâûì êëåòêàì, âñåì îêðóæàþùèì èõ äîáàâëÿåì 1 ñîñåäà
                if (wn >= 0 && wn < size) {
                    for (int hn = (h - 1); hn <= (h + 1); hn++) {
                        if (hn >= 0 && hn < size) {
                            if (wn != w || hn != h)
                                amount[wn][hn].add_neig();
                        }
                    }
                }
            }
        }


        for (int wn = 0; wn < size; wn++) {
            if (wn >= 0 && wn < size) {
                for (int hn = 0; hn <= size; hn++) {
                    if (hn >= 0 && hn < size) {
                        if (amount[wn][hn].GetType() == true) {
                            if ((amount[wn][hn].GetState() == true) && (amount[wn][hn].GetNeigh() > 4)) {
                                amount[wn][hn] = new Zombie_Cell();

                            } else if (amount[wn][hn].change() == true) {
                                Next_alive.add(new Coord(wn, hn));
                            }
                            amount[wn][hn].zer_neig();
                        } else if (amount[wn][hn].change() == false) {
                            amount[wn][hn] = new Cell();
                            //amount[wn][hn]->SetState(1);
                            //Next_alive.emplace_back(wn, hn);


                        }
                    }
                }

            }


            Curr_alive.removeAllElements();
            for (int y = 0; y < Next_alive.size(); y++) {
                Curr_alive.add(new Coord(Next_alive.get(y).line, Next_alive.get(y).coll));
            }

            Next_alive.removeAllElements();


        }


    }


}